#!/bin/env python

"""
This is a VERY OLD version of Python script, as this is called using the old 
Weblogic scripting tool.

Please DO NOT call this script directly. Instead, call sibling 'soa_health.py'.
"""

import sys
from time import sleep

def _resume(server):
    try:
        cmo.resume()
        sleep(1)
        state = cmo.getState()
        if state == 'ADMIN':
            print("%s: server couldn't be resumed; still in 'ADMIN' state." % server)
        elif state == 'RUNNING':
            print("%s: server was resumed." % server)
        else:
            print("%s: server couldn't be resumed; currently in '%s' state." % (server, state))
    except Exception, e:
        print("%s: ERROR occured when trying to resume server: %s." % (server, str(e)))

def _check_soa(server, port, uname, upass, fix):
    complete_server_url = "t3://%s:%s" % (server, str(port))
    try:
        upass_list = upass.split(':')
        complete_pass = "".join([chr(int(c)) for c in upass_list])
        connect(uname, complete_pass, complete_server_url)
        serverRuntime()
        state = cmo.getState()
        print("%s: state: %s" % (server, state))
        if state == 'ADMIN':
            if fix == '1':
                print("%s: trying to resume server..." % server)
                _resume(server)
        elif state != 'RUNNING':
            print("%s: nothing to do." % server)
    except Exception, e:
        print("%s: ERROR: %s" % (server, str(e)))

server = sys.argv[1]
port = sys.argv[2]
uname = sys.argv[3]
upass = sys.argv[4]
fix = sys.argv[5]
_check_soa(server, port, uname, upass, fix)
