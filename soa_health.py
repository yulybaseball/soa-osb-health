#!/bin/env python

"""
Checks the state of SOA/OSB instances and admin servers; updates the state to 
'RUNNING' if it is requested to do it.

Usage:
./soa_health.py [-f] [<service:datacenter:side> <...>] [-e <email_address>]

Arguments:
-f: Indicates if this script should intent forcing the instance to resume.
service: Either specific name of a server, or one of these for a complete 
    service: isoa, psoa, eosb, iosb, posb or ofs. If only a server is 
    specified, only such server will be checked. If a complete service is 
    specified, this script will check all servers associated to that service. 
    If no service is specified, this script will check all SOA/OSB servers in 
    PROD environment. Please see services names below.
datacenter: Either letter 'a' (ATL) or 'm' (MIA), without the quotes.
side: Either letter 'a' (side A) or 'b' (side B), without the quotes.
-e: Email address to send a notification to.

Plase note the services to check are always from PROD environment. For 
checking others, specify server names.

If only the name of the service is specified, this script will check all 
servers associated to that service in PROD in the primary data center.

For checking admin servers, use the same service name, and add 'adm' at the 
end: isoaadm, psoaadm, eosbadm, iosbadm, posbadm, ofsadm

Some samples:

./soa_health.py isoa:m:b  # ISOA in MIA side B
./soa_health.py eosb  # all EOSB in primary DC
./soa_health.py posb:a:a isoa  # POSB in ATL side A, and all ISOA in primary DC
./soa_health.py  # all SOA/OSB servers (excluding ADMINs)
./soa_health.py dp-ldtsoainta2  # only server dp-ldtsoainta2
./soa_health.py -f ofs:m:a  # OFS in MIA side A, and resuming them if in ADMIN
./soa_health.py -e pss@tracfone.com  # check all, and notify by email
./soa_health.py isoaadm  # check ISOA admins in primary DC
./soa_health.py ofsadm:m:a  # check OFS admin in MIA side A
"""

import getopt
from multiprocessing.pool import ThreadPool as Pool
from os import path, system
import sys
from threading import Lock

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
CONF_FILE_NAME = path.join(EXEC_DIR, 'conf', 'conf.json')

sys.path.append(path.dirname(EXEC_DIR))

from pssapi.utils import conf, conn, util
from pssapi.cmdexe import cmd

log2email = None
s_print_lock = None

COMMAND = "java weblogic.WLST -skipWLSModuleScanning _soa.py {server} {port} {uname} {upass} {fix}"
OPTIONS = "e:f"

def _init():
    """Initialize globals."""
    global log2email
    global s_print_lock
    log2email = []
    s_print_lock = Lock()

def _quit(error=None):
    """Print error (if not None), and exit this script."""
    if error:
        print("Error: {0}".format(error))
    print("Finished... bye.")
    exit()

def _s_print(a):
    """Thread safe print function."""
    with s_print_lock:
        print(a)
        log2email.append(a)

def _get_conf():
    """Return a dictionary with the conf values defined in a json file."""
    config = conf.get_conf(CONF_FILE_NAME)
    if isinstance(config, str):
        _quit(config)
    return config

def _get_credentials(credentials_conf):
    """Return a dictionary with the credentials which definition is specify 
    in 'credentials_conf'.
    """
    admin_user_name_var = credentials_conf['admin_user_name']
    aun = next(iter(admin_user_name_var))
    bs = admin_user_name_var[aun]
    admin_user_name = conn.val_from_bash(aun, bashscript=bs)
    admin_pass_var = credentials_conf['admin_pass']
    ap = next(iter(admin_pass_var))
    bs = admin_pass_var[ap]
    admin_pass = conn.val_from_bash(ap, bashscript=bs)
    return {'uname': admin_user_name, 'upass': admin_pass}

def _services2check(values_arguments):
    """Create and return a list of dictionaries containing the services passed 
    in as arguments to this script.
    """
    services2check = []
    if len(values_arguments) > 0:
        def _create_servic2check_dict(sv_list):
            s2c_dict = {}
            _pos_val = {'0': 'service', '1': 'dc', '2': 'side'}
            for pos, val in enumerate(sv_list):
                s2c_dict[_pos_val[str(pos)]] = val
            return s2c_dict
        for s2check in values_arguments:
            sv_list = s2check.split(':')
            if len(sv_list) == 1:
                services2check.append(sv_list[0])
            else:
                sv_dict = _create_servic2check_dict(sv_list)
                services2check.append(sv_dict)
    return services2check

def _get_all_servers(all_servers_conf):
    """Return all SOA/OSB servers, as defined by a variable in 
    'all_servers_conf' dictionary.
    """
    all_servers_var = next(iter(all_servers_conf))
    all_servers_bash = all_servers_conf[all_servers_var]
    return conn.val_from_bash(all_servers_var, bashscript=all_servers_bash, 
                              split=' ')

def _servers_service_primary_dc(servers_conf, service2check):
    """Search the servers contained into 'service2check', when it isn't a 
    dictionary."""
    servers_keys = servers_conf.keys()
    if service2check in servers_keys:
        # complete service in primary data center
        svar_template = next(iter(servers_conf['primary_dc_service']))
        service_bash_file = servers_conf['primary_dc_service'][svar_template]
        dc = conn.val_from_bash('DEFAULT_LOCATION')
        service_var_values = {'dc': dc, 'service': service2check.upper()}
        service_final_var = svar_template.format(**service_var_values)
        return conn.val_from_bash(service_final_var, split=' ',
                                  bashscript=service_bash_file)
    return [service2check]

def _get_servers2check(servers_conf, services2check):
    """Return the list of servers to check/fix.

    Keyword arguments:
    servers_conf -- Dictionary containing the definition of the variable and 
    the bash script of the service servers.
    services2check -- Services passed in as arguments to this script.
    """
    if not services2check:
        return _get_all_servers(servers_conf['all'])
    servers = []
    dc = {'m': 'MIA', 'a': 'ATL'}
    side = {'a': 'A', 'b': 'B'}
    soa_osb_keys = servers_conf.keys()
    for service2check in services2check:
        if not isinstance(service2check, dict):
            # service2check could be either a server2check or a service2check
            # in the primary DC
            service_servers = _servers_service_primary_dc(servers_conf, 
                                                          service2check)
        else:
            service = service2check['service']
            if service in soa_osb_keys:
                # get the list of servers for 'service'
                service_var = next(iter(servers_conf[service]))
                service_bash_file = servers_conf[service][service_var]
                _dc = dc[service2check['dc']]
                _side = side[service2check['side']]
                service_final_var = service_var.format(dc=_dc, side=_side)
                service_servers = conn.val_from_bash(service_final_var, 
                                                bashscript=service_bash_file, 
                                                split=' ')
        servers = list(set(servers) | set(service_servers))
    return servers

def _filter_stdout(stdout, server):
    """Filter and print only the lines in stdout that contains 
    'servers' name.
    """
    output_list = stdout.strip().split('\n')
    output_template = "{0}: ".format(server)
    _s_print("\n".join(
        [output for output in output_list if output_template in output]
    ))

def _server_checker(server, ports, credentials, options, env_path):
    """Check SOA/OSB instance state in 'server'.
    This is executed by a thread (not the application's main one).
    """
    import subprocess
    setenv_command = ". {0}".format(env_path)
    fix = '1' if '-f' in options.keys() else '0'
    uname = credentials['uname']
    upass = credentials['upass']
    encoded_pass = ":".join(str(ord(c)) for c in upass)
    # to set the port it is used a hack:
    # any server name containing 'adm' will be treated as admin server
    port = ports['admin_port'] if 'adm' in server.lower() else ports['port']
    params = {
        'server': server, 'uname': uname, 
        'upass': encoded_pass, 
        'fix': fix, 'port': port
    }
    command = COMMAND.format(**params)
    final_command = "{0}; {1}".format(setenv_command, command)
    p = subprocess.Popen(final_command, shell=True, stdin=subprocess.PIPE, 
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, dummy = p.communicate()
    _filter_stdout(stdout, server)

def _check_servers(servers, ports, credentials, options, env_path):
    """Create the pool of threads for checking servers."""
    servers_count = len(servers)
    if servers_count > 1:
        msg_servers = ("Checking servers {0}\nPLEASE be patient...").\
            format(", ".join(servers))
    elif servers_count == 1:
        msg_servers = "Checking {0} ...".format(servers[0])
    else:
        _quit()
    pool = Pool(servers_count)
    print(msg_servers)
    for server in servers:
        pool.apply_async(_server_checker, (server, ports, credentials, 
                                           options, env_path))
    pool.close()
    pool.join()

def _notify(params, servers, options):
    """Send email notification if defined in the options."""
    if log2email and '-e' in options.keys() and options['-e']:
        email_address = options['-e']
        print("Trying to send email to {0}".format(email_address))
        try:
            from pssapi.emailer import emailer
            subject = "SOA/OSB Health - Check result"
            body = ("Script was called with these params: {0}\n").\
                format(" ".join(params))
            body += "Servers: {0}\n\n".format(", ".join(servers))
            body += ("\n".join(log2email))
            emailer.send_email(subject, body, dest=[email_address])
        except Exception as e:
            print("Error: {0}".format(str(e)))

def _refined_options(options):
    """Return a dictionary with the options ans theirs values."""
    refined_options = {}
    for option in options:
        refined_options[option[0]] = option[1]
    return refined_options

def _main():
    fullparams = sys.argv
    params = fullparams[1:]
    _init()
    try:
        options, values = getopt.gnu_getopt(params, OPTIONS)
        config = _get_conf()
        credentials = _get_credentials(config['credentials'])
        services2check = _services2check(values)
        servers = _get_servers2check(config['servers'], services2check)
        env_path = config['env_path']
        ports = {'port': config['port'], 'admin_port': config['admin_port']}
        servers = sorted(servers)
        refined_options = _refined_options(options)
        _check_servers(servers, ports, credentials, refined_options, env_path)
        _notify(params, servers, refined_options)
    except Exception as e:
        _quit(str(e))

if __name__ == "__main__":
    _main()
